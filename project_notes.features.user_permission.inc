<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function project_notes_user_default_permissions() {
  $permissions = array();

  // Exported permission: create project_notes content
  $permissions['create project_notes content'] = array(
    'name' => 'create project_notes content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'authenticated user',
      '2' => 'manager',
    ),
  );

  // Exported permission: delete any project_notes content
  $permissions['delete any project_notes content'] = array(
    'name' => 'delete any project_notes content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'manager',
    ),
  );

  // Exported permission: delete own project_notes content
  $permissions['delete own project_notes content'] = array(
    'name' => 'delete own project_notes content',
    'roles' => array(),
  );

  // Exported permission: edit any project_notes content
  $permissions['edit any project_notes content'] = array(
    'name' => 'edit any project_notes content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'authenticated user',
      '2' => 'manager',
    ),
  );

  // Exported permission: edit own project_notes content
  $permissions['edit own project_notes content'] = array(
    'name' => 'edit own project_notes content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  return $permissions;
}
