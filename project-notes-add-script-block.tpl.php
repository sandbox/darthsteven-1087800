<div class="project-notes-add-script">
  <div class="no-script">To use the project notes greasemonkey script, you must have javascript enabled.</div>
  <div class="show-via-js"><?php print $add_script_link; ?> the project notes script.</div><div class="show-via-js">To use the project notes greasemonkey script you need to have greasemonkey installed, if you don't you can install it here: <a href="https://addons.mozilla.org/en/firefox/addon/748">GreaseMonkey extension for Firefox.</a></div>
</div>
