<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function project_notes_context_default_contexts() {
  $export = array();
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'project_notes_listing';
  $context->description = '';
  $context->tag = 'Project notes';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'project_notes:page_1' => 'project_notes:page_1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'project_notes-add_script' => array(
          'module' => 'project_notes',
          'delta' => 'add_script',
          'region' => 'right',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Project notes');

  $export['project_notes_listing'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'project_notes_notes';
  $context->description = '';
  $context->tag = 'Project notes';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'project_notes' => 'project_notes',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'project_notes-add_script' => array(
          'module' => 'project_notes',
          'delta' => 'add_script',
          'region' => 'right',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Project notes');

  $export['project_notes_notes'] = $context;
  return $export;
}
