<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function project_notes_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => 3);
  }
  elseif ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function project_notes_node_info() {
  $items = array(
    'project_notes' => array(
      'name' => t('Project notes'),
      'module' => 'features',
      'description' => t('Stores notes about projects on Drupal.org.'),
      'has_title' => '1',
      'title_label' => t('Project name'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function project_notes_views_api() {
  return array(
    'api' => '2',
  );
}
