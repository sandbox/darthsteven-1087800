// ==UserScript==
// @name           Atrium: Drupal Project Notes
// @namespace      atrium
// @include        http://drupal.org/project/*
// ==/UserScript==
/* */

// A function that loads jQuery and calls a callback function when jQuery has finished loading
function addJQuery(callback) {
  var script = document.createElement("script");
  script.setAttribute("src", "http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js");
  script.addEventListener(
    'load',
    function() {
      var script = document.createElement("script");
      script.textContent = "(" + callback.toString() + ")();";
      document.body.appendChild(script);
    },
    false
  );
  document.body.appendChild(script);
}

// Load jQuery and required extensions
function main() {

  // jQuery's own JSONP implementation is buggy so we use the .jsonp extension instead.
  // jquery.jsonp 2.1.4 (c)2010 Julian Aubourg | MIT License
  // http://code.google.com/p/jquery-jsonp/
  (function(e,b){function d(){}function t(C){c=[C]}function m(C){f.insertBefore(C,f.firstChild)}function l(E,C,D){return E&&E.apply(C.context||C,D)}function k(C){return/\?/.test(C)?"&":"?"}var n="async",s="charset",q="",A="error",r="_jqjsp",w="on",o=w+"click",p=w+A,a=w+"load",i=w+"readystatechange",z="removeChild",g="<script/>",v="success",y="timeout",x=e.browser,f=e("head")[0]||document.documentElement,u={},j=0,c,h={callback:r,url:location.href};function B(C){C=e.extend({},h,C);var Q=C.complete,E=C.dataFilter,M=C.callbackParameter,R=C.callback,G=C.cache,J=C.pageCache,I=C.charset,D=C.url,L=C.data,P=C.timeout,O,K=0,H=d;C.abort=function(){!K++&&H()};if(l(C.beforeSend,C,[C])===false||K){return C}D=D||q;L=L?((typeof L)=="string"?L:e.param(L,C.traditional)):q;D+=L?(k(D)+L):q;M&&(D+=k(D)+encodeURIComponent(M)+"=?");!G&&!J&&(D+=k(D)+"_"+(new Date()).getTime()+"=");D=D.replace(/=\?(&|$)/,"="+R+"$1");function N(S){!K++&&b(function(){H();J&&(u[D]={s:[S]});E&&(S=E.apply(C,[S]));l(C.success,C,[S,v]);l(Q,C,[C,v])},0)}function F(S){!K++&&b(function(){H();J&&S!=y&&(u[D]=S);l(C.error,C,[C,S]);l(Q,C,[C,S])},0)}J&&(O=u[D])?(O.s?N(O.s[0]):F(O)):b(function(T,S,U){if(!K){U=P>0&&b(function(){F(y)},P);H=function(){U&&clearTimeout(U);T[i]=T[o]=T[a]=T[p]=null;f[z](T);S&&f[z](S)};window[R]=t;T=e(g)[0];T.id=r+j++;if(I){T[s]=I}function V(W){(T[o]||d)();W=c;c=undefined;W?N(W[0]):F(A)}if(x.msie){T.event=o;T.htmlFor=T.id;T[i]=function(){/loaded|complete/.test(T.readyState)&&V()}}else{T[p]=T[a]=V;x.opera?((S=e(g)[0]).text="jQuery('#"+T.id+"')[0]."+p+"()"):T[n]=n}T.src=D;m(T);S&&m(S)}},0);return C}B.setup=function(C){e.extend(h,C)};e.jsonp=B})(jQuery,setTimeout);

	/*
	 * jQuery Expander plugin
	 * Version 0.4  (12/09/2008)
	 * @requires jQuery v1.1.1+
	 *
	 * Dual licensed under the MIT and GPL licenses:
	 * http://www.opensource.org/licenses/mit-license.php
	 * http://www.gnu.org/licenses/gpl.html
	 *
	 */
	(function($) {

	  $.fn.expander = function(options) {

	    var opts = $.extend({}, $.fn.expander.defaults, options);
	    var delayedCollapse;
	    return this.each(function() {
	      var $this = $(this);
	      var o = $.meta ? $.extend({}, opts, $this.data()) : opts;
	     	var cleanedTag, startTags, endTags;
	     	var allText = $this.html();
	     	var startText = allText.slice(0, o.slicePoint).replace(/\w+$/,'');
	     	startTags = startText.match(/<\w[^>]*>/g);
	   	  if (startTags) {startText = allText.slice(0,o.slicePoint + startTags.join('').length).replace(/\w+$/,'');}

	     	if (startText.lastIndexOf('<') > startText.lastIndexOf('>') ) {
	     	  startText = startText.slice(0,startText.lastIndexOf('<'));
	     	}
	     	var endText = allText.slice(startText.length);
	     	// create necessary expand/collapse elements if they don't already exist
	   	  if (!$('span.details', this).length) {
	        // end script if text length isn't long enough.
	       	if ( endText.replace(/\s+$/,'').split(' ').length < o.widow ) { return; }
	       	// otherwise, continue...
	       	if (endText.indexOf('</') > -1) {
	         	endTags = endText.match(/<(\/)?[^>]*>/g);
	          for (var i=0; i < endTags.length; i++) {

	            if (endTags[i].indexOf('</') > -1) {
	              var startTag, startTagExists = false;
	              for (var j=0; j < i; j++) {
	                startTag = endTags[j].slice(0, endTags[j].indexOf(' ')).replace(/(\w)$/,'$1>');
	                if (startTag == rSlash(endTags[i])) {
	                  startTagExists = true;
	                }
	              }
	              if (!startTagExists) {
	                startText = startText + endTags[i];
	                var matched = false;
	                for (var s=startTags.length - 1; s >= 0; s--) {
	                  if (startTags[s].slice(0, startTags[s].indexOf(' ')).replace(/(\w)$/,'$1>') == rSlash(endTags[i])
	                  && matched == false) {
	                    cleanedTag = cleanedTag ? startTags[s] + cleanedTag : startTags[s];
	                    matched = true;
	                  }
	                };
	              }
	            }
	          }

	          endText = cleanedTag && cleanedTag + endText || endText;
	        }
	     	  $this.html([
	     		startText,
	     		'<span class="read-more">',
	     		o.expandPrefix,
	       		'<a href="#">',
	       		  o.expandText,
	       		'</a>',
	        '</span>',
	     		'<span class="details">',
	     		  endText,
	     		'</span>'
	     		].join('')
	     	  );
	      }
	      var $thisDetails = $('span.details', this),
	        $readMore = $('span.read-more', this);
	   	  $thisDetails.hide();
	 	    $readMore.find('a').click(function() {
	 	      $readMore.hide();

	 	      if (o.expandEffect === 'show' && !o.expandSpeed) {
	          o.beforeExpand($this);
	 	        $thisDetails.show();
	          o.afterExpand($this);
	          delayCollapse(o, $thisDetails);
	 	      } else {
	          o.beforeExpand($this);
	 	        $thisDetails[o.expandEffect](o.expandSpeed, function() {
	            $thisDetails.css({zoom: ''});
	            o.afterExpand($this);
	            delayCollapse(o, $thisDetails);
	 	        });
	 	      }
	        return false;
	 	    });
	      if (o.userCollapse) {
	        $this
	        .find('span.details').append('<span class="re-collapse">' + o.userCollapsePrefix + '<a href="#">' + o.userCollapseText + '</a></span>');
	        $this.find('span.re-collapse a').click(function() {

	          clearTimeout(delayedCollapse);
	          var $detailsCollapsed = $(this).parents('span.details');
	          reCollapse($detailsCollapsed);
	          o.onCollapse($this, true);
	          return false;
	        });
	      }
	    });
	    function reCollapse(el) {
	       el.hide()
	        .prev('span.read-more').show();
	    }
	    function delayCollapse(option, $collapseEl) {
	      if (option.collapseTimer) {
	        delayedCollapse = setTimeout(function() {
	          reCollapse($collapseEl);
	          option.onCollapse($collapseEl.parent(), false);
	          },
	          option.collapseTimer
	        );
	      }
	    }
	    function rSlash(rString) {
	      return rString.replace(/\//,'');
	    }
	  };
	    // plugin defaults
	  $.fn.expander.defaults = {
	    slicePoint:       170,  // the number of characters at which the contents will be sliced into two parts.
	                            // Note: any tag names in the HTML that appear inside the sliced element before
	                            // the slicePoint will be counted along with the text characters.
	    widow:            4,  // a threshold of sorts for whether to initially hide/collapse part of the element's contents.
	                          // If after slicing the contents in two there are fewer words in the second part than
	                          // the value set by widow, we won't bother hiding/collapsing anything.
	    expandText:       'read more', // text displayed in a link instead of the hidden part of the element.
	                                      // clicking this will expand/show the hidden/collapsed text
	    expandPrefix:     '&hellip; ',
	    collapseTimer:    0, // number of milliseconds after text has been expanded at which to collapse the text again
	    expandEffect:     'fadeIn',
	    expandSpeed:      '',   // speed in milliseconds of the animation effect for expanding the text
	    userCollapse:     false, // allow the user to re-collapse the expanded text.
	    userCollapseText: '[collapse expanded text]',  // text to use for the link to re-collapse the text
	    userCollapsePrefix: ' ',
	    beforeExpand: function($thisEl) {},
	    afterExpand: function($thisEl) {},
	    onCollapse: function($thisEl, byUser) {}
	  };
	})(jQuery);

	/////////////////////////////////////////////////////////////////////////////

  // Inject our JSONP parser into the DOM using contentEval
  // http://wiki.greasespot.net/Content_Script_Injection
  (function() {
    function contentEval(source) {
      // Check for function input.
      if ('function' == typeof source) {
        // Execute this function with no arguments, by adding parentheses.
        // One set around the function, required for valid syntax, and a
        // second empty set calls the surrounded function.
        source = '(' + source + ')();'
      }

      // Create a script node holding this  source code.
      var script = document.createElement('script');
      script.setAttribute("type", "application/javascript");
      script.textContent = source;

      // Insert the script node into the page, so it will run, and immediately
      // remove it to clean up.
      document.body.appendChild(script);
      document.body.removeChild(script);
    }

    contentEval(function() {
      npn = {
        callback: function(returnData) {

          if (npn.version < returnData.settings.latest_version) {
            var doUpdate = window.confirm('A new version of Project notes is available. Do you want to update? You will not be able to view notes until you do.');
            if (doUpdate) {
              window.open('<?php print $base_url;?>' + 'project_notes', 'project_notes');
            }
            // Don't process any further if there is a new version.
            return;
          }

          if (returnData.projects) {
            var pnotes = returnData.projects[0];
          }

          var
            style = { 'padding' : '4px 9px' , 'background-color' : '#EEEEEE',  '-moz-border-radius': '0 0 5px 5px'},
            // TODO: Regex redundancy! Cannot define once due to sandbox variable scope restrictions.
            // Update server to always pass requested project name in JSON (requires additional Views modules):
            header = $('<h3>').attr('style', 'color: white; background-color: ' + returnData.settings.group_color +'; font-weight: bold; margin-top: 0; margin-bottom: 0; padding: 4px 9px; -moz-border-radius: 5px 5px 0 0;').html('Notes for <em>' + window.location.href.match(/http:\/\/drupal\.org\/project(|\/issues)?\/(\w+)/)[2] + '</em>'),
            notes = {},
            exportability = {},
            stability = {},
            notfound = {},
            editlink = {},
            createlink = {},
            innercontent = {},
            content = {};

          content = $('<div>').css({'margin': '1em 0'});
          content.append(header);

          if (pnotes && pnotes.project) {
            innercontent = $('<div>').css(style);
            for (var field in pnotes.project) {
              if (field.substr(0, 6) == 'field ') {
                var fieldLabel = field.substr(6);
                var item = $('<p>').append($('<label>').text(fieldLabel)).append($('<div>').html(pnotes.project[field]));
                innercontent.append(item);
              }
            }
            editlink = $('<p>').css('text-align', 'right').append($('<a>').html('Edit these notes').attr('href', npn.base_url + pnotes.project.edit_url).click(function(){ window.open(this.href); return false; }));
            innercontent.append(editlink);
          }

          // TODO: Views Datasource doesn't support attachments, but if it did it would be nice to append the server info to the
          // project info and would be passed regardless of whether any project info was found or not.
          // That way we wouldn't have to hard to server URL and name of the project notes content type but could instead
          // grab them from the JSONP.
          // The server URL is redundant (see below) but we cannot access it due to variable sandbox scope restrictions.
          // We could make an additional request just to grab this info but it seems like a waste of HTTP requests
          // even though there's browser caching.

          else {
            notfound = $('<p>').text('No notes found');
            createlink = $('<p>').css('text-align', 'right').append($('<a>').text('Add notes for this project').attr('href', '<?php print $add_url; ?>?edit[title]=' + npn.projname).click(function(){ window.open(this.href); return false; }));
            innercontent = $('<div>').css(style).append(notfound).append(createlink);
          }

          content.append(innercontent);

          $('#content').prepend(content);

        },
        feed: '<?php print $feed_url; ?>',
        version: <?php print $version; ?>,
        base_url: '<?php print $base_url; ?>'
      };
    });
  })();

  // Check URL for matches and initiate JSONP request
  (function() {
    // Matches http://drupal.org/project(|/issues)/*
    npn.projname = window.location.href.match(/http:\/\/drupal\.org\/project(|\/issues)?\/(\w+)/)[2];

    if (npn.projname && npn.projname != 'modules' && npn.projname != 'themes') {
      // Initiate XD JSONP request to get settings.
      $.jsonp({
        url: npn.feed + '/' + npn.projname,
        pageCache: true,
        callbackParameter: 'callback',
        callback: 'npn.callback'
      });
    }
  })();

}

// Load jQuery and execute the main function
addJQuery(main);
