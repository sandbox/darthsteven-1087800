
(function($) {
  Drupal.behaviors.project_notes_add_script = function(context) {
    $(context).find('.project-notes-add-script:not(.project-notes-add-script-processed)')
      .each(function() {
        // Hide the no script stuff.
        $(this).find('.no-script').hide();
        $(this).find('.show-via-js').show();
        
      })
      .addClass('project-notes-add-script-processed');
  }
})(jQuery);